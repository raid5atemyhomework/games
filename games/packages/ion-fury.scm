;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Timotej Lazar <timotej.lazar@araneo.si>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages ion-fury)
  #:use-module (guix packages)
  #:use-module (guix build-system copy)
  #:use-module (guix utils)
  #:use-module (gnu packages compression)
  #:use-module (games gog-download)
  #:use-module (nongnu packages game-development)
  #:use-module (nonguix licenses))

(define-public gog-ion-fury
  (let ((buildno "33710"))
    (package
      (name "gog-ion-fury")
      (version "1.02")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://ion_maiden_game/en3installer0")
         (file-name (string-append "ion_fury_"
                                   (string-replace-substring version "." "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32 "06k71l45xci7qchxl8lad74wv1h5gw785gsng5m2d21b8ahjwwqz"))))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         '(("data/noarch/game/" "share/ion-fury/" #:include-regexp ("fury\\..*"))
           ("data/noarch/game/legal.txt" "share/doc/ion-fury/")
           ("data/noarch/support/icon.png" "share/pixmaps/ion-fury.png"))
         ;; Import (nonguix build utils) for make-desktop-entry-file.
         #:imported-modules ((nonguix build utils) ,@%copy-build-system-modules)
         #:modules ((guix build copy-build-system)
                    (guix build utils)
                    (nonguix build utils))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda _
               (invoke (which "makeself_safeextract")
                       "--mojo" (assoc-ref %build-inputs "source"))
               (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
               #t))
           (add-after 'install 'install-wrapper
             ;; Install launcher script and .desktop file.
             (lambda _
               (let* ((out (assoc-ref %outputs "out"))
                      (wrapper (string-append out "/bin/ion-fury"))
                      (config "~/.config/fury")) ; Hardcoded in the engine.
                 (mkdir-p (dirname wrapper))
                 (with-output-to-file wrapper
                   (lambda _
                     (format #t "#!~a/bin/sh~%" (which "sh"))
                     ;; Log file is written to the current working directory.
                     (format #t "mkdir -p ~a~%" config)
                     (format #t "cd ~a~%" config)
                     (format #t "exec ~a/bin/fury -j ~a \"$@\"~%"
                             (assoc-ref %build-inputs "fury")
                             (string-append out "/share/ion-fury"))))
                 (chmod wrapper #o755)
                 (make-desktop-entry-file
                  (string-append out "/share/applications/ion-fury.desktop")
                  #:name "Ion Fury"
                  #:exec wrapper
                  #:icon (string-append out "/share/pixmaps/ion-fury.png")
                  #:comment "First-person shooter game"
                  #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("makeself-safeextract" ,makeself-safeextract)))
      (inputs
       `(("fury" ,fury)))
      (home-page "http://www.ionfury.com")
      (synopsis "First-person shooter video game")
      (description "Ion Fury is a first-person shooter video game running on
the improved version of the Build engine, originally used with Duke Nukem 3D,
Blood and Shadow Warrior.  Both graphics and gameplay are styled after those
games, with fast-paced action, puzzles and abundant easter eggs.

The player assumes the role of Shelly “Bombshell” Harrison, a bomb disposal
expert for the Global Defense Force fighting against the transhumanist cult
mastermind Dr. Jadus Heskel and his cybernetic army.")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
